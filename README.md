-----------------------------------------------------------------------------------------
Task #1: Create a knowledge base of your choosing (favorite movies, books, foods, etc.) This knowledge base must include at least 25 items (facts and rules) and you must come up with at least 10 questions that you can answer using this knowledge base.
-----------------------------------------------------------------------------------------
Task#2: Represent the following knowledge base in Prolog: A is akiller. B and C are married. D is dead. C kills everyone who gives B a foot massage. B loves everyone who is a good dancer. E eats anything that is nutritious or tasty.
-----------------------------------------------------------------------------------------

Instructions for compiling and running this Erlang file

After downloading all the files in this project and storing them in your directory, there are 2 ways to compile and run:


1. Using git bash:
. Log in to the directory by command: cd PHUONG_HAI_NGUYEN_PROLOGI
. Type this command to log in to SWI-Prolog : swipl
. Test if the erl file has no error existing: ['FILENAME']. (Ex: ['index'].)
. Compile and run each function by command: FUNCTION_NAME(arg). (Ex: function1(X). then type ; to see next possible results.)
-> the solutions for all functions will show up as the example below

2. Using PowerShell in Windows system
. Log in to the directory by command that contains files: cd /d YOURDIRECTORY (Ex: cd \d C:\Windows)
. Type this command to log in to SWI-Prolog: swipl
. Test if the erl file has no error existing: ['FILENAME']. (Ex: ['index'].)
. Compile and run each function by command: FUNCTION_NAME(arg). (Ex: function1(X). then type ; to see next possible results.)
-> the solutions for all functions will show up as the example below



                            Example Solutions for all functions
                            
//                          OPENING WINDOWS POWERSHELL  
// =============================================================================
Windows PowerShell
Copyright (C) 2014 Microsoft Corporation. All rights reserved.

PS C:\Users\ADMIN> cd .\PHUONG_HAI_NGUYEN_PROLOGI
PS C:\Users\ADMIN\PHUONG_HAI_NGUYEN_PROLOGI> swipl
Welcome to SWI-Prolog (threaded, 64 bits, version 7.6.0)
SWI-Prolog comes with ABSOLUTELY NO WARRANTY. This is free software.
Please run ?- license. for legal details.

For online help and background, visit http://www.swi-prolog.org
For built-in help, use ?- help(Topic). or ?- apropos(Word).

1 ?- ['index'].
true.

%% What food can be a meal?
2 ?- meal(X).       
X = burger ;
X = sandwich ;
X = pizza ;
X = ramen ;
X = steak ;
X = chicken ;
X = beef ;
X = eggs ;
X = pork ;
X = cookies ;
X = icecream ;
X = candy ;
X = pudding ;
X = cereal ;
X = bread.

%% is steak a food?
3 ?- food(steak).       
true.

%% What kind of lunch we have?
4 ?- lunch(X).          
X = ramen ;
X = burger ;
X = beef ;
X = pork ;
X = steak ;
X = chicken.

%% What kind of breakfast we have?
5 ?- breakfast(X).      
X = cereal ;
X = bread ;
X = sandwich ;
X = eggs.

%% What kind of brunch we have?
6 ?- brunch(X).        
X = cookies ;
X = candy.

%% What kind of dinner we have?
7 ?- dinner(X).         
X = pork ;
X = steak ;
X = chicken ;
X = pizza.

%% What kind of meal that can be for lunch and also for dinner(if there is any)?
8 ?- meal(X),lunch(X),dinner(X).        
X = steak ;
X = chicken ;
X = pork ;

%% What kind of meal that can be for breakfast and also for dinner(if there is any)?
9 ?- meal(X),breakfast(X),dinner(X).
false.

%% Is icecream a dinner?
10 ?- dinner(icecream).
false.

%% Is icecream a dessert?
11 ?- dessert(icecream).
true.