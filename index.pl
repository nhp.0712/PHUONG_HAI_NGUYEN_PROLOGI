%%  Task #1: Create a knowledge base of your choosing (favorite movies, books, foods, etc.)
%% This knowledge base must include at least 25 items (facts and rules) and you must come up 
%% with at least 10 questions that you can answer using this knowledge base.

%%=============================================
%% 					Facts
food(burger). 				%% burger is a food
food(sandwich). 			%% sandwich is a food
food(pizza). 				%% pizza is a food
food(ramen).				%% ramen is a food
food(steak).				%% steak is a food
food(chicken).				%% chicken is a food
food(beef).					%% beef is a food
food(eggs).					%% eggs is a food
food(pork).					%% pork is a food
food(cookies).				%% cookies is a food
food(icecream).				%% icecream is a food
food(candy).				%% candy is a food
food(pudding).				%% pudding is a food
food(cereal).				%% cereal is a food
food(bread).				%% bread is a food
breakfast(cereal).			%% cereal is a breakfast
breakfast(bread).			%% bread is a breakfast
breakfast(sandwich).		%% sandwich is a breakfast
breakfast(eggs).			%% eggs is a breakfast
brunch(cookies).			%% cookies is a brunch
brunch(candy).				%% candy is a brunch
lunch(ramen). 				%% ramen is a lunch
lunch(burger). 				%% burger is a lunch
lunch(beef).				%% beef is a lunch
lunch(pork). 				%% pork is a lunch
lunch(steak). 				%% steak is a lunch
lunch(chicken). 			%% chicken is a lunch
dinner(pork). 				%% pork is a dinner
dinner(steak). 				%% steak is a dinner
dinner(chicken). 			%% chicken is a dinner
dinner(pizza). 				%% pizza is a dinner
dessert(icecream).			%% icecream is a dessert
dessert(pudding).			%% pudding is a dessert

%% 					Rules
meal(X) :- food(X). 		%% Every food is a meal OR anything is a meal if it is a food



%% Task#2: Re present the following knowledge base in Prolog: 
%% • Aisakiller.
%% • BandCaremarried. 
%% • Disdead. 
%% • CkillseveryonewhogivesBafootmassage. 
%% • Bloveseveryonewhoisagooddancer. 
%% • Eeatsanythingthatisnutritiousortasty.
%%=============================================
%% A is a killer
killer(A).
%% B and C are married
married(B, C).
%% D is dead
dead(D).
%% C kills everyone who gives B a footmassage
kills(C, X) :- footMassage(X, B). 			
%% B loves everyone who is a good dancer
loves(B, X) :- goodDancer(X). 	 
%% E eats anything that is nutritious or tasty
eats(E, X) :- nutritious(X); tasty(X).